from conans import ConanFile, tools
import shutil

class AvirConan(ConanFile):
    name           = "avir"
    license        = "MIT"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-avir/"
    homepage       = "https://github.com/avaneev/avir"
    description    = "High-quality pro image resizing / scaling C++ library, image resize "
    topics         = ("image resize", "header only")
    no_copy_source = True
    # No settings/options are necessary, this is header only

    def source(self):
        '''retrieval of the source code here. Remember you can also put the code
        in the folder and use exports instead of retrieving it with this
        source() method
        '''
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("avir-{}".format(self.version), "avir")

    def package(self):
        self.copy("*.h", dst="include", src="avir", keep_path=False)

    def package_id(self):
        self.info.header_only()